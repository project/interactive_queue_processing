<?php

namespace Drupal\Tests\interactive_queue_processing\Functional;

use Drupal\interactive_queue_processing_test\Plugin\QueueWorker\NodeCreateQueueWorker;
use Drupal\node\NodeInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Base functionality for functional tests.
 */
abstract class InteractiveQueueProcessingBrowserTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['interactive_queue_processing_test'];

  /**
   * Adjust config to activate queue processing.
   *
   * @param bool $status
   *   Status for processing. Defaults to TRUE, which will allow the queues to
   *   process.
   */
  protected function activateProcessing($status = TRUE) {
    $config = $this->config('interactive_queue_processing.settings');
    $config->set('active', $status);
    $config->save();
  }

  /**
   * Add permission to anonymous user to allow them to process queues.
   */
  protected function allowAnonProcessing() {
    $role = Role::load('anonymous');
    $role->grantPermission('interactively process queues');
    $role->save();
  }

  /**
   * Verifies a set of nodes exist.
   *
   * @param int $start
   *   The beginning title.
   * @param int $end
   *   The ending title.
   */
  protected function assertNumericNodes($start, $end) {
    for ($nid = $start; $nid <= $end; $nid++) {
      $this->assertInstanceOf(NodeInterface::class, $this->drupalGetNodeByTitle($nid));
    }
  }

  /**
   * Verifies a set of nodes does not exist.
   *
   * @param int $start
   *   The beginning title.
   * @param int $end
   *   The ending title.
   */
  protected function assertNoNumericNodes($start, $end) {
    for ($nid = $start; $nid <= $end; $nid++) {
      $this->assertFalse($this->drupalGetNodeByTitle($nid));
    }
  }

  /**
   * Queue nodes to be created.
   *
   * @param int $start
   *   The beginning title.
   * @param int $end
   *   The ending title.
   * @param array $values
   *   Extra values to be added to the node. Optional, defaults to no values.
   */
  protected function queueNumericNodes($start, $end, array $values = []) {
    $queue = \Drupal::queue(NodeCreateQueueWorker::ID);
    for ($nid = $start; $nid <= $end; $nid++) {
      $values['title'] = $nid;
      $queue->createItem($values);
    }
  }

  /**
   * Processes the queues via AJAX endpoint.
   */
  protected function processQueues() {
    $this->drupalGet('interactive-queue-processing/ajax');
    $this->assertSession()->statusCodeEquals(200);
  }

}
