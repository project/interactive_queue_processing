<?php

namespace Drupal\Tests\interactive_queue_processing\Functional;

use Drupal\interactive_queue_processing_test\Plugin\QueueWorker\NodeCreateQueueWorker;

/**
 * Functional tests.
 *
 * @group interactive_queue_processing
 */
class InteractiveQueueProcessingBrowserTest extends InteractiveQueueProcessingBrowserTestBase {

  /**
   * Tests the "active" configuration for processing.
   */
  public function testActivation() {
    $this->allowAnonProcessing();
    $this->drupalCreateContentType(['type' => NodeCreateQueueWorker::TYPE]);
    $this->queueNumericNodes(1, 3);

    // Not active.
    $this->processQueues();
    $this->assertNoNumericNodes(1, 3);

    // Activated.
    $this->activateProcessing();
    $this->processQueues();
    $this->assertNumericNodes(1, 3);
  }

  /**
   * Tests that the AJAX processing endpoint is secured by permission.
   */
  public function testProcessQueuePermission() {
    $this->activateProcessing();
    $this->drupalCreateContentType(['type' => NodeCreateQueueWorker::TYPE]);
    $this->queueNumericNodes(1, 3);

    // Anonymous.
    $this->drupalGet('interactive-queue-processing/ajax');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertNoNumericNodes(1, 3);

    // Authenticated w/permission.
    $account = $this->drupalCreateUser(['interactively process queues']);
    $this->drupalLogin($account);
    $this->drupalGet('interactive-queue-processing/ajax');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertNumericNodes(1, 3);
  }

  /**
   * Tests that the max items configuration is respected.
   */
  public function testMaxItems() {
    $this->activateProcessing();
    $this->allowAnonProcessing();
    $this->drupalCreateContentType(['type' => NodeCreateQueueWorker::TYPE]);
    $this->queueNumericNodes(1, 20);

    // Limit 3.
    $config = $this->config('interactive_queue_processing.settings');
    $config->set('max_items', 3);
    $config->save();
    $this->processQueues();
    $this->assertNumericNodes(1, 3);
    $this->assertNoNumericNodes(4, 20);

    // Limit 5.
    $config->set('max_items', 5);
    $config->save();
    $this->processQueues();
    $this->assertNumericNodes(1, 8);
    $this->assertNoNumericNodes(9, 20);

    // Unlimited.
    $config->set('max_items', -1);
    $config->save();
    $this->processQueues();
    $this->assertNumericNodes(1, 20);
  }

  /**
   * Tests processing time limit.
   */
  public function testProcessingTime() {
    $this->activateProcessing();
    $this->allowAnonProcessing();
    $this->drupalCreateContentType(['type' => NodeCreateQueueWorker::TYPE]);
    $this->queueNumericNodes(1, 20, ['sleep' => 1000]);

    // Limit 3.
    $config = $this->config('interactive_queue_processing.settings');
    $config->set('processing_time', 3);
    $config->save();
    $this->processQueues();
    $this->assertNumericNodes(1, 3);
    $this->assertNoNumericNodes(4, 20);

    // Limit 5.
    $config->set('processing_time', 5);
    $config->save();
    $this->processQueues();
    $this->assertNumericNodes(1, 8);
    $this->assertNoNumericNodes(9, 20);
  }

}
