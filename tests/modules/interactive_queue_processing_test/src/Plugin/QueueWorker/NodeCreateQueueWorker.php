<?php

namespace Drupal\interactive_queue_processing_test\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Creates a node.
 *
 * @QueueWorker(
 *   id = "interactive_queue_processing_test_node_create",
 *   title = @Translation("Node create"),
 *   cron = {"time" = 60}
 * )
 */
class NodeCreateQueueWorker extends QueueWorkerBase {

  const ID = 'interactive_queue_processing_test_node_create';

  const TYPE = 'iqp_test';

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!is_array($data)) {
      $data = [];
    }

    if (!empty($data['sleep'])) {
      usleep($data['sleep'] * 1000);
      unset($data['sleep']);
    }

    $data += [
      'title' => \Drupal::service('password_generator')->generate(10),
      'type' => static::TYPE,
      'status' => NodeInterface::PUBLISHED,
      'uid' => 1,
    ];

    $node = Node::create($data);
    $node->save();
  }

}
