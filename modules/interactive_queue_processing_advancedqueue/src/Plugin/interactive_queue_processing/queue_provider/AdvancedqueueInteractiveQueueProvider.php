<?php

namespace Drupal\interactive_queue_processing_advancedqueue\Plugin\interactive_queue_processing\queue_provider;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\Processor;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\interactive_queue_processing\Plugin\interactive_queue_processing\queue_provider\InteractiveQueueProviderBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interactive queue provider for advanced queue.
 *
 * @InteractiveQueueProvider(
 *   id = "advancedqueue",
 * )
 */
class AdvancedqueueInteractiveQueueProvider extends InteractiveQueueProviderBase {

  /**
   * Advanced queue processor service.
   *
   * @var \Drupal\advancedqueue\Processor
   */
  protected $advancedqueueProcessor;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a CoreInteractiveQueueProvider plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\advancedqueue\Processor $advancedqueueProcessor
   *   Advanced queue processor service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Processor $advancedqueueProcessor, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('advancedqueue.processor'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQueues() {
    $queues = $this->entityTypeManager->getStorage('advancedqueue_queue')
      ->loadMultiple();
    return array_keys($queues);
  }

  /**
   * {@inheritdoc}
   */
  public function getQueueInfo($queue_id) {
    return $this->entityTypeManager->getStorage('advancedqueue_queue')
      ->load($queue_id);
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($queue_id) {
    /** @var \Drupal\advancedqueue\Entity\Queue $queue */
    $queue = $this->entityTypeManager->getStorage('advancedqueue_queue')
      ->load($queue_id);

    if (!$queue) {
      return static::QUEUE_FAIL;
    }

    if (!$job = $queue->getBackend()->claimJob()) {
      return static::NO_ITEMS;
    }

    $result = $this->advancedqueueProcessor->processJob($job, $queue);

    return $result->getState() == Job::STATE_SUCCESS ? static::ITEM_PROCESS_SUCCESSFUL : static::ITEM_PROCESS_FAIL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCount($queue_id) {
    /** @var \Drupal\advancedqueue\Entity\Queue $queue */
    if ($queue = $this->entityTypeManager->getStorage('advancedqueue_queue')->load($queue_id)) {
      return $queue->getBackend()->countJobs()['queued'] ?? 0;
    }
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function garbageCollection($queue_id) {
    /** @var \Drupal\advancedqueue\Entity\Queue $queue */
    if (!$queue = $this->entityTypeManager->getStorage('advancedqueue_queue')->load($queue_id)) {
      return;
    }
    $queue->getBackend()->cleanupQueue();
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessing($queue_id) {
    /** @var \Drupal\advancedqueue\Entity\Queue $queue */
    if (!$queue = $this->entityTypeManager->getStorage('advancedqueue_queue')->load($queue_id)) {
      return 0;
    }
    return $queue->getBackend()->countJobs()['processing'] ?? 0;
  }

}
