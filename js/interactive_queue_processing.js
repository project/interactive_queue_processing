(function (Drupal, $) {

  let init = false;
  let timeout = false;

  Drupal.behaviors.interactiveQueueProcessing = {
    attach: function (context, settings) {
      // Initialize only once per page load.
      if (init) {
        return;
      }
      init = true;

      // If we've got items to process, process immediately, otherwise wait for
      // long interval.
      if (settings.interactive_queue_processing.counts > 0) {
        doAjax(settings);
      }
      else {
        setNextAjax(settings, settings.interactive_queue_processing.waitLong);
      }
    }
  };

  // Performs the AJAX call.
  function doAjax(settings) {
    const url = settings.path.baseUrl + 'interactive-queue-processing/ajax';
    $.ajax(url).done(function (data) {
      // AJA response indicates if and how long to wait before retrying.
      if (data.result.retry) {
        setNextAjax(settings, data.result.retry);
      }
    });
  }

  // Set a timeout to perform the next AJAX call.
  function setNextAjax(settings, pause) {
    // Don't adjust timeout if there is currently a pending timeout.
    if (timeout) {
      return;
    }

    timeout = setTimeout(function () {
      // When the timeout is executed, unset the timeout and do the AJAX.
      timeout = false;
      doAjax(settings);
    }, pause);
  }

})(Drupal, jQuery)
