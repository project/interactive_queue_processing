<?php

namespace Drupal\interactive_queue_processing;

/**
 * Interface for interactive queue.
 */
interface InteractiveQueueProviderInterface {

  const NO_ITEMS = 0;

  const ITEM_PROCESS_FAIL = 1;

  const ITEM_PROCESS_SUCCESSFUL = 2;

  const QUEUE_FAIL = 3;

  /**
   * Get the plugin ID.
   *
   * @return string
   *   The ID of the plugin.
   */
  public function id();

  /**
   * Perform garbage collection for the queues.
   *
   * @param string $queue_id
   *   The queue ID.
   *
   * @return int
   *   The count of items in the queue ready to be processed.
   */
  public function garbageCollection($queue_id);

  /**
   * Get counts for a queue provided by this plugin.
   *
   * @param string $queue_id
   *   The queue ID.
   *
   * @return int
   *   The count of items in the queue ready to be processed.
   */
  public function getCount($queue_id);

  /**
   * Get the number of items currently processing in the queue.
   *
   * @param string $queue_id
   *   The queue ID.
   *
   * @return int|null
   *   The count of items in the queue currently processing, or NULL if the
   *   queue doesn't support counting processing items.
   */
  public function getProcessing($queue_id);

  /**
   * Get the queues provided by this plugin.
   *
   * @return array
   *   An array containing the queues provided by this plugin.
   */
  public function getQueues();

  /**
   * Get the info regarding a queue provided by this plugin.
   *
   * @param string $queue_id
   *   The queue ID.
   *
   * @return array
   *   An array containing information about the queue.
   */
  public function getQueueInfo($queue_id);

  /**
   * Attempt to obtain and process an item from the queue.
   *
   * @param string $queue_id
   *   The queue ID.
   *
   * @return int
   *   Indicates the result of the processing.
   */
  public function processItem($queue_id);

}
