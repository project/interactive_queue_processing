<?php

namespace Drupal\interactive_queue_processing\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Queue processing controller.
 */
class Process extends ControllerBase {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The queue manager service.
   *
   * @var \Drupal\interactive_queue_processing\QueueManager
   */
  protected $queueManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $controller = parent::create($container);
    $controller->time = $container->get('datetime.time');
    $controller->queueManager = $container->get('interactive_queue_processing.queue_manager');
    return $controller;
  }

  /**
   * Handles AJAX queue processing.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response.
   */
  public function ajax() {
    $start_time = $this->time->getCurrentMicroTime();

    $result = $this->queueManager->processQueues();

    $end_time = $this->time->getCurrentMicroTime();

    $response = new JsonResponse();
    $response->setData([
      'startTime' => $start_time,
      'endTime' => $end_time,
      'elapsedTime' => $end_time - $start_time,
      'remaining' => $this->queueManager->queueCounts(),
      'result' => $result,
    ]);
    return $response;
  }

}
