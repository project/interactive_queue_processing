<?php

namespace Drupal\interactive_queue_processing;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Various queue management functionality.
 */
class QueueManager {

  const PROCESSING_COMPLETE = 0;

  const PROCESSING_NOT_ACTIVE = 1;

  const PROCESSING_TIMEOUT = 2;

  const PROCESSING_MAX_ITEMS = 3;

  const RETRY_DELAY_SHORT = 1000;

  const RETRY_DELAY_LONG = 30000;

  const RETRY_NEVER = NULL;

  /**
   * All queue definitions.
   *
   * @var array|null
   */
  protected $queues = NULL;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Interactive queue provider plugin manager.
   *
   * @var \Drupal\interactive_queue_processing\InteractiveQueueProviderPluginManager
   */
  protected $queueProviderPluginManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * QueueManager constructor.
   *
   * @param InteractiveQueueProviderPluginManager $queueProviderPluginManager
   *   The interactive queue provider plugin manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   */
  public function __construct(InteractiveQueueProviderPluginManager $queueProviderPluginManager, TimeInterface $time, EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $configFactory) {
    $this->queueProviderPluginManager = $queueProviderPluginManager;
    $this->time = $time;
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
  }

  /**
   * Gets the queue definitions.
   *
   * @return array
   *   Queue definitions.
   */
  public function getQueueDefinitions() {
    if ($this->queues !== NULL) {
      return $this->queues;
    }

    $config = $this->configFactory->get('interactive_queue_processing.settings');
    $defaults = [
      'max_concurrent' => $config->get('default_queue_max_concurrent'),
      'priority' => $config->get('default_queue_priority'),
      'status' => $config->get('default_queue_status'),
    ];

    $this->queues = [];

    // Load each provider.
    foreach ($this->queueProviderPluginManager->getDefinitions() as $definition) {
      /** @var \Drupal\interactive_queue_processing\InteractiveQueueProviderInterface $plugin */
      $plugin = $this->queueProviderPluginManager->createInstance($definition['id']);

      // Obtain data for provided queues.
      foreach ($plugin->getQueues() as $queue_id) {
        $id = "{$plugin->id()}:$queue_id";
        $this->queues[$id] = [
          'plugin_id' => $plugin->id(),
          'queue_id' => $queue_id,
          'info' => $plugin->getQueueInfo($queue_id),
        ];

        // Merge in config.
        if ($queue_config = $config->get("queues.$id")) {
          $this->queues[$id]['max_concurrent'] = $queue_config['max_concurrent'];
          $this->queues[$id]['priority'] = $queue_config['priority'];
          $this->queues[$id]['status'] = $queue_config['status'];
        }

        // Merge in defaults.
        $this->queues[$id] += $defaults;
      }
    }

    usort($this->queues, '\Drupal\interactive_queue_processing\QueueManager::sortQueues');
    return $this->queues;
  }

  /**
   * Sort callback for ordering of queues.
   *
   * @param array $a
   *   Queue definition.
   * @param array $b
   *   Queue definition.
   *
   * @return int
   *   Sort indicator.
   */
  protected static function sortQueues(array $a, array $b) {
    // Primary sort, priority (highest first).
    if ($a['priority'] < $b['priority']) {
      return 1;
    }
    elseif ($a['priority'] > $b['priority']) {
      return -1;
    }

    // Secondary sort, queue_id (alpha).
    if ($a['queue_id'] > $b['queue_id']) {
      return 1;
    }
    elseif ($a['queue_id'] < $b['queue_id']) {
      return -1;
    }

    // Tertiary sort, queue plugin (alpha).
    if ($a['plugin_id'] > $b['plugin_id']) {
      return 1;
    }
    elseif ($a['plugin_id'] < $b['plugin_id']) {
      return -1;
    }

    return 0;
  }

  /**
   * Processes queues.
   *
   * @return array
   *   Data pertaining to the queue processing.
   */
  public function processQueues() {
    $config = $this->configFactory->get('interactive_queue_processing.settings');

    // We'll only process for ~the configured amount of time.
    $start = $this->time->getCurrentMicroTime();
    $end = $start + $config->get('processing_time');

    $max_items = $config->get('max_items');
    $items_processed = 0;
    $return = [
      'result' => NULL,
      'processed' => $items_processed,
      'concurrency_limit' => FALSE,
      'retry' => static::RETRY_NEVER,
    ];

    if (!$config->get('active')) {
      $return['result'] = static::PROCESSING_NOT_ACTIVE;
      return $return;
    }

    foreach ($this->getQueueDefinitions() as $definition) {
      // Only enabled queues, already ordered by priority.
      if (empty($definition['status'])) {
        continue;
      }

      /** @var \Drupal\interactive_queue_processing\InteractiveQueueProviderInterface $plugin */
      $plugin = $this->queueProviderPluginManager->createInstance($definition['plugin_id']);

      // Reset expired items.
      $plugin->garbageCollection($definition['queue_id']);

      // Skip processing if we're already up to the maximum concurrency.
      if ($definition['max_concurrent'] > 0) {
        if ($plugin->getProcessing($definition['queue_id']) >= $definition['max_concurrent']) {
          $return['concurrency_limit'] = TRUE;
          continue;
        }
      }

      do {
        // Attempt to process an item.
        $result = $plugin->processItem($definition['queue_id']);
        if ($result != InteractiveQueueProviderInterface::NO_ITEMS) {
          $items_processed++;
          $return['processed'] = $items_processed;
        }

        if ($max_items > 0 && $items_processed >= $max_items) {
          $return['result'] = static::PROCESSING_MAX_ITEMS;
          $return['retry'] = static::RETRY_DELAY_SHORT;
          return $return;
        }

        // If we've come to the end time for our processing, stop.
        if ($this->time->getCurrentMicroTime() >= $end) {
          $return['result'] = static::PROCESSING_TIMEOUT;
          $return['retry'] = static::RETRY_DELAY_SHORT;
          return $return;
        }

        if ($result == InteractiveQueueProviderInterface::QUEUE_FAIL) {
          break;
        }

      } while ($result != InteractiveQueueProviderInterface::NO_ITEMS);
    }

    $return['result'] = static::PROCESSING_COMPLETE;
    $return['retry'] = static::RETRY_DELAY_LONG;

    return $return;
  }

  /**
   * Counts items in queues.
   *
   * @param bool $sum
   *   Return the sum of all remaining items. Optional, defaults to TRUE.
   *
   * @return array
   *   Number of items in all queues remaining to be processed, or an array
   *   of queue information including counts.
   */
  public function queueCounts($sum = TRUE) {
    $count = [];

    foreach ($this->getQueueDefinitions() as $info) {
      if (empty($info['status'])) {
        continue;
      }

      $row = [
        'name' => $info['queue_id'],
        'type' => $info['plugin_id'],
      ];

      /** @var \Drupal\interactive_queue_processing\InteractiveQueueProviderInterface $plugin */
      $plugin = $this->queueProviderPluginManager->createInstance($info['plugin_id']);
      $row['count'] = $plugin->getCount($info['queue_id']);
      $count[] = $row;
    }

    if ($sum) {
      $count = array_map(function ($row) {
        return $row['count'];
      }, $count);
      $count = array_sum($count);
    }

    return $count;
  }

}
