<?php

namespace Drupal\interactive_queue_processing\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\interactive_queue_processing\QueueManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Interactive Queue Processing.
 */
class InteractiveQueueProcessingSettingsForm extends ConfigFormBase {

  /**
   * The queue manager service.
   *
   * @var \Drupal\interactive_queue_processing\QueueManager
   */
  protected $queueManager;

  /**
   * Constructs an Interactive Queue Processing settings form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\interactive_queue_processing\QueueManager $queueManager
   *   Queue manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, QueueManager $queueManager) {
    parent::__construct($config_factory);
    $this->queueManager = $queueManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('interactive_queue_processing.queue_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['interactive_queue_processing.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'interactive_queue_processing_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('interactive_queue_processing.settings');

    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General'),
    ];

    $form['general']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $settings->get('active'),
    ];

    $form['general']['processing_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Processing time'),
      '#default_value' => $settings->get('processing_time'),
      '#required' => TRUE,
      '#description' => $this->t('Approximate maximum time of processing for each interactive request.'),
    ];

    $form['general']['max_items'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max items'),
      '#default_value' => $settings->get('max_items'),
      '#required' => TRUE,
      '#description' => $this->t('Maximum number of items to process in one run. Use -1 to indicate no limit.'),
    ];

    $form['queues'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Queues'),
      '#tree' => TRUE,
    ];

    foreach ($this->queueManager->getQueueDefinitions() as $item) {
      $form['queues'][] = $this->buildQueueRow($item);
    }

    $form['defaults'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Defaults'),
    ];

    $form['defaults']['default_queue_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $settings->get('default_queue_status'),
      '#description' => $this->t('Newly-added queues will utilize this status.'),
    ];

    $form['defaults']['default_queue_priority'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Priority'),
      '#default_value' => $settings->get('default_queue_priority'),
      '#required' => TRUE,
      '#description' => $this->t('Newly-added queues will utilize this priority.'),
    ];

    $form['defaults']['default_queue_max_concurrent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max concurrent'),
      '#default_value' => $settings->get('default_queue_max_concurrent'),
      '#required' => TRUE,
      '#description' => $this->t('Newly-added queues will utilize this maximum concurrency. Use 0 or -1 to indicate no limit.'),
    ];

    $form = parent::buildForm($form, $form_state);
    $form['#theme'] = 'interactive_queue_processing_settings_form';
    return $form;
  }

  /**
   * Get a form row for a queue's configuration.
   *
   * @param array $item
   *   A queue's configuration.
   *
   * @return array
   *   Form structure for the queue's configuration.
   */
  protected function buildQueueRow(array $item) {
    $row['id'] = [
      '#type' => 'value',
      '#value' => "{$item['plugin_id']}:{$item['queue_id']}",
    ];

    $row['name'] = [
      '#plain_text' => $item['queue_id'],
    ];

    $row['type'] = [
      '#plain_text' => $item['plugin_id'],
    ];

    $row['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $item['status'],
    ];

    $row['priority'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Priority'),
      '#default_value' => $item['priority'],
    ];

    $row['max_concurrent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max concurrent'),
      '#default_value' => $item['max_concurrent'],
    ];

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('interactive_queue_processing.settings');

    $queues = [];
    foreach ($form_state->getValue('queues') as $queue) {
      $parts = explode(':', $queue['id']);
      $queues[$queue['id']]['plugin_id'] = array_shift($parts);
      $queues[$queue['id']]['queue_id'] = array_shift($parts);
      $queues[$queue['id']] += $queue;
      unset($queues[$queue['id']]['id']);
    }
    ksort($queues);

    $settings->set('active', $form_state->getValue('active'))
      ->set('processing_time', $form_state->getValue('processing_time'))
      ->set('max_items', $form_state->getValue('max_items'))
      ->set('queues', $queues)
      ->set('default_queue_status', $form_state->getValue('default_queue_status'))
      ->set('default_queue_priority', $form_state->getValue('default_queue_priority'))
      ->set('default_queue_max_concurrent', $form_state->getValue('default_queue_max_concurrent'));

    $settings->save();

    parent::submitForm($form, $form_state);
  }

}
