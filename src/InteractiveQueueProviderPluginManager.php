<?php

namespace Drupal\interactive_queue_processing;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manager for InteractiveQueueProvider plugins.
 */
class InteractiveQueueProviderPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new InteractiveQueueProviderPluginManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/interactive_queue_processing/queue_provider', $namespaces, $module_handler, 'Drupal\interactive_queue_processing\InteractiveQueueProviderInterface', 'Drupal\interactive_queue_processing\Annotation\InteractiveQueueProvider');
    $this->alterInfo('interactive_queue_processing_queue_info');
    $this->setCacheBackend($cache_backend, 'interactive_queue_processing_queue_info');
  }

}
