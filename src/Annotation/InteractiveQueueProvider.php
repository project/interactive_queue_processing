<?php

namespace Drupal\interactive_queue_processing\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation object for interactive queue provider plugins.
 *
 * Plugin Namespace: Plugin\interactive_queue_processing\queue_provider.
 *
 * @Annotation
 */
class InteractiveQueueProvider extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
