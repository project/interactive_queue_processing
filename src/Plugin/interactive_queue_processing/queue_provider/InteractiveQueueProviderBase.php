<?php

namespace Drupal\interactive_queue_processing\Plugin\interactive_queue_processing\queue_provider;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\interactive_queue_processing\InteractiveQueueProviderInterface;

/**
 * Base interactive queue provider.
 */
abstract class InteractiveQueueProviderBase extends PluginBase implements ContainerFactoryPluginInterface, InteractiveQueueProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->pluginDefinition['id'];
  }

}
