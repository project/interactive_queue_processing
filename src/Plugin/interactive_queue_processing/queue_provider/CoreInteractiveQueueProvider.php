<?php

namespace Drupal\interactive_queue_processing\Plugin\interactive_queue_processing\queue_provider;

use Drupal\Core\Database\Connection;
use Drupal\Core\Queue\DatabaseQueue;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueGarbageCollectionInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\RequeueException;
use Drupal\Core\Queue\SuspendQueueException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interactive queue provider for core queue implementation.
 *
 * @InteractiveQueueProvider(
 *   id = "core",
 * )
 */
class CoreInteractiveQueueProvider extends InteractiveQueueProviderBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The queue worker plugin manager service.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * Constructs a CoreInteractiveQueueProvider plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queueWorkerManager
   *   The queue worker plugin manager service.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, QueueWorkerManagerInterface $queueWorkerManager, QueueFactory $queueFactory, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->queueWorkerManager = $queueWorkerManager;
    $this->queueFactory = $queueFactory;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.queue_worker'),
      $container->get('queue'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQueues() {
    return array_filter(array_map(function ($info) {
      return isset($info['cron']) ? $info['id'] : FALSE;
    }, $this->queueWorkerManager->getDefinitions()));
  }

  /**
   * {@inheritdoc}
   */
  public function getQueueInfo($queue_name) {
    return $this->queueWorkerManager->getDefinition($queue_name);
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($queue_id) {
    $queue_definition = $this->getQueueInfo($queue_id);

    $queue = $this->queueFactory->get($queue_id);
    $lease_time = isset($queue_definition['queue']['cron']['time']) ?: NULL;
    if (!$item = $queue->claimItem($lease_time)) {
      return static::NO_ITEMS;
    }

    try {
      $queue_worker = $this->queueWorkerManager->createInstance($queue_id);
      $queue_worker->processItem($item->data);
      $queue->deleteItem($item);
    }
    catch (RequeueException $e) {
      // The worker requested the task be immediately requeued.
      $queue->releaseItem($item);
    }
    catch (SuspendQueueException $e) {
      // If the worker indicates there is a problem with the whole queue,
      // release the item and skip to the next queue.
      $queue->releaseItem($item);
      watchdog_exception('interactive_queue_processing', $e);
      return static::QUEUE_FAIL;
    }
    catch (\Exception $e) {
      // In case of any other kind of exception, log it and leave the item
      // in the queue to be processed again later.
      watchdog_exception('interactive_queue_processing', $e);
      return static::ITEM_PROCESS_FAIL;
    }

    return static::ITEM_PROCESS_SUCCESSFUL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCount($queue_id) {
    $queue = $this->queueFactory->get($queue_id);
    return $queue->numberOfItems();
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessing($queue_id) {
    $queue = $this->queueFactory->get($queue_id);
    if (!$queue instanceof DatabaseQueue) {
      return NULL;
    }

    if (!$this->connection->schema()->tableExists(DatabaseQueue::TABLE_NAME)) {
      return 0;
    }

    $select = $this->connection->select(DatabaseQueue::TABLE_NAME, 'q')
      ->condition('name', $queue_id)
      ->condition('expire', 0, '<>');

    return $select->countQuery()->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function garbageCollection($queue_id) {
    $queue = $this->queueFactory->get($queue_id);
    if ($queue instanceof QueueGarbageCollectionInterface) {
      $queue->garbageCollection();
    }
  }

}
